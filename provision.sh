#!/bin/bash --login
​
sudo apt-get update
​
# Install Apache2
sudo apt-get -y install apache2
sudo sh -c 'echo "ServerName localhost" >> /etc/apache2/conf.d/name'
​
# Install MySQL: password is 'root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
sudo apt-get -y install mysql-server
sudo apt-get -y install libmysqlclient-dev
​
# Install PHP
sudo apt-get -y install php5
sudo apt-get -y install libapache2-mod-php5
sudo apt-get -y install php5-mysql
sudo apt-get -y install php-pear
sudo apt-get -y install php5-xdebug
sudo apt-get -y install php5-gd
sudo apt-get -y install php5-curl
​
# Install useful tools
sudo apt-get -y install curl
sudo apt-get -y install unzip
sudo apt-get -y install git
sudo apt-get -y install vim
​
# activate mod_rewrite
if [ ! -L /etc/apache2/mods-enabled/rewrite.load ]; then
    sudo ln -s /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/rewrite.load
fi
​
# load vhosts
sudo rm -rf /etc/apache2/sites-enabled
sudo ln -s /vagrant/conf/sites-available /etc/apache2/sites-enabled
​
# Add Vagrant to www-data group
sudo usermod -a -G www-data  vagrant
​
mkdir /vagrant/www
​
# Restart servers
sudo service apache2 restart
sudo service mysql restart
​
# Automatically change to the source code directory on SSH connection
echo "cd /vagrant" >> /home/vagrant/.bashrc
